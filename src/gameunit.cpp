#include "gameunit.h"
#include <typeinfo>

const float GameUnit::GRAVITY = 0.25f;
const float GameUnit::FRICTION = 0.1f;
const float GameUnit::COLLISION_RESOLUTION = 0.025f;

GameUnit::GameUnit(GameEntityManager &gem, sf::RenderWindow &w, Type t) :
    GameEntity(gem,w,t),
    gravity(1.0f),
    direction(1.0f,0.0f,0.0f),
    hp(1.0f),
    target(NULL),
    faction(FACTION_NEUTRAL)
{
}
GameUnit::~GameUnit()
{
    for(unsigned int c = 0; c < targettedBy.size(); c++)
    {
        targettedBy[c]->target = NULL;
    }
}
void GameUnit::step()
{
    if(geometry.bottomZ() > 0.0f)
    {
        geometry.vel.z -= gravity * GRAVITY;
    }
    geometry.pos += geometry.vel;
    if(geometry.bottomZ() < 0.0f)
    {
        geometry.pos.z = geometry.broadphase_radius;
        geometry.vel.z = 0.0f;
    }
    if(geometry.bottomZ() <= 0.0f)
    {
        float mag = magnitudeVector3f(geometry.vel);
        if(mag > 0.0f)
        {
            mag *= FRICTION;
            geometry.vel = normalizeVector3f(geometry.vel) * mag;
        }
    }
    if(hp <= 0.0f)
    {
        nullify = true;
    }
}
void GameUnit::collideWith(GameEntity *e)
{
    //default behavior that should occur when two units are walking around on the field
    if(e->getType() >= GameUnit::TYPE_UNIT && e->getType() < GameUnit::END_TYPE_UNIT)
    {
        sf::Vector3f difference = geometry.pos - e->geometry.pos;
        difference.z = 0.0f;
        if(magnitudeVector3f(difference) == 0.0f)
        {
            difference = sf::Vector3f(rand()/(float)RAND_MAX,rand()/(float)RAND_MAX,rand()/(float)RAND_MAX);
            if(magnitudeVector3f(difference) == 0.0f)
            {
                difference = sf::Vector3f(1.0f,0.0f,0.0f);
            }
            else
            {
                difference = normalizeVector3f(difference);
            }
        }
        else
        {
            difference = normalizeVector3f(difference);
        }
        float totalRadius = e->geometry.broadphase_radius + geometry.broadphase_radius;
        difference *= totalRadius * COLLISION_RESOLUTION;
        //geometry.pos = e->geometry.pos + difference;
        geometry.vel += difference;
    }
}

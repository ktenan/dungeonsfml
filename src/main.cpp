#include <SFML/Graphics.hpp>
#include "resourcemanager.h"
#include "gamestate.h"
#include "globals.h"
void processEvents(sf::RenderWindow &window);
State** createStates(sf::RenderWindow &window);
void processStates(State **states, State::StateID &currState, sf::RenderWindow &window);
int main()
{
    sf::RenderWindow window(sf::VideoMode(WINDOW_W, WINDOW_H), WINDOW_TITLE);
    window.setFramerateLimit(APPLICATION_FRAMERATE);
    if(!ResourceManager::load())
    {
        return EXIT_FAILURE;
    }
    State **states = createStates(window);
    State::StateID currState = State::STATE_GAME;
    states[currState]->init();
    while (window.isOpen())
    {
        processEvents(window);
        window.clear();
        processStates(states, currState, window);
        window.display();
    }
    return EXIT_SUCCESS;
}
void processEvents(sf::RenderWindow &window)
{
    sf::Event event;
    while (window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            window.close();
        }
    }
}
State** createStates(sf::RenderWindow &window)
{
    State **retVal = new State*[State::NUM_STATES];
    for(int s = 0; s < State::NUM_STATES; s++)
    {
        switch(s)
        {
            case State::STATE_GAME:
                retVal[s] = new GameState(window, retVal);
                break;
            default:
                retVal[s] = NULL;
                break;
        }
    }
    return retVal;
}
void processStates(State **states, State::StateID &currState, sf::RenderWindow &window)
{
    State::StateID runVal = states[currState]->run();
    if(runVal == State::NUM_STATES)
    {
        window.close();
        return;
    }
    g_fpsTimer.restart();
    states[currState]->draw();
    if(runVal != currState)
    {
        states[currState]->cleanup();
        states[runVal]->init();
        currState = runVal;
    }
}

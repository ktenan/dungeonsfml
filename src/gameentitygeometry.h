#ifndef GAMEENTITYGEOMETRY_H
#define GAMEENTITYGEOMETRY_H

#include <SFML/System.hpp>

class GameEntityGeometry
{
    public:
        bool testBroadPhase(GameEntityGeometry &g);
        float bottomZ();
    public:
        sf::Vector3f pos;
        sf::Vector3f vel;
        float broadphase_radius;
};

#endif // GAMEENTITYGEOMETRY_H

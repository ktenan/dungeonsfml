#include "gamecamera.h"

const sf::Vector2f& GameCamera::getSize() const
{
    return size;
}
void GameCamera::setW(float w)
{
    size.x = w;
    size.y = (size.x / WINDOW_ASPECT) * ARENA_VIEW_ASPECT;
}
void GameCamera::setH(float h)
{
    size.y = h;
    size.x = (size.y / ARENA_VIEW_ASPECT) * WINDOW_ASPECT;
}

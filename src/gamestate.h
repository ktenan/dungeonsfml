#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "globals.h"
#include "state.h"
#include "gameentitymanager.h"
#include "gamecamera.h"
#include "gameunitisaac.h"

class GameState : public State
{
    public:
        GameState(sf::RenderWindow &w, State **s);
        virtual StateID run();
        virtual void draw();
        virtual void init();
        virtual void cleanup();
    private:
        void drawBG();
        void drawMainScene();
        void drawMinimap();
    private:
        GameEntityManager gem;
        sf::View mainCam;
        sf::View bgCam;
        sf::View minimapCam;
        GameCamera camera;
        sf::Sprite sprBG;
        sf::Sprite sprDebugBoundsB;
        sf::Sprite sprDebugBoundsF;
        sf::Sprite sprMinimapRing;
        sf::Sprite sprMinimapCam;
        sf::Sprite sprMinimapBG;
        GameUnit *isaac;
};

#endif // GAMESTATE_H

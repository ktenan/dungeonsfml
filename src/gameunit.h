#ifndef GAMEUNIT_H
#define GAMEUNIT_H

#include "gameentity.h"

class GameUnit : public GameEntity
{
    public:
        enum Faction
        {
            FACTION_NEUTRAL,
            FACTION_PLAYER,
            FACTION_ENEMY
        };
        static const float GRAVITY;
        static const float FRICTION;
        static const float COLLISION_RESOLUTION;
    public:
        GameUnit(GameEntityManager &gem, sf::RenderWindow &w, Type t);
        virtual ~GameUnit();
        virtual void step();
        virtual void draw(const GameCamera &camera) = 0;
        virtual void collideWith(GameEntity *e);
    public:
        float gravity;
        sf::Vector3f direction;
        float hp;
        GameUnit *target;
        std::vector<GameUnit*> targettedBy;
        Faction faction;
};

#endif // GAMEUNIT_H

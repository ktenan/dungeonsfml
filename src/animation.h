#ifndef ANIMATION_H
#define ANIMATION_H

#include <SFML/Graphics.hpp>

class Animation
{
    public:
        Animation(sf::RenderWindow &w, sf::Texture *texArray, int numFrames,
                  int ticksPF, sf::Vector2f origin, bool oscill = false);
        void animate();
        void draw();
    public:
        sf::Vector2f position;
        bool flipX;
        sf::Vector2f scale;
    private:
        sf::RenderWindow &window;
        int frames;
        int currFrame;
        std::vector<sf::Sprite> sprites;
        int ticksPerFrame;
        int ticksLeft;
        bool oscillate;
        bool oscillateDirection;
};

#endif // ANIMATION_H

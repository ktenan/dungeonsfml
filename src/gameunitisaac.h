#ifndef GAMEUNITISAAC_H
#define GAMEUNITISAAC_H

#include "gameunit.h"
#include "animation.h"

class GameUnitIsaac : public GameUnit
{
    public:
        static const float RADIUS;
    public:
        GameUnitIsaac(GameEntityManager &gem, sf::RenderWindow &w);
        virtual ~GameUnitIsaac();
        virtual void step();
        virtual void draw(const GameCamera &camera);
        virtual void collideWith(GameEntity *e);
    private:
        sf::Sprite sprShadow;
        Animation animSwordAttackBack;
        Animation animSwordAttackFront;
        Animation animSwordBack;
        Animation animSwordStartcastBack;
        Animation animSwordCastBack;
        Animation animSwordStartcastFront;
        Animation animSwordCastFront;
        Animation animSwordDownedBack;
        Animation animSwordDownedFront;
        Animation animSwordFront;
        Animation animSwordHitBack;
        Animation animSwordHitFront;
};

#endif // GAMEUNITISAAC_H

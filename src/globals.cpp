#include "globals.h"
#include <cmath>
#include <cassert>

const int WINDOW_W = 800;
const int WINDOW_H = 600;
const float WINDOW_ASPECT = (float)WINDOW_W / WINDOW_H;
const std::string WINDOW_TITLE = "DungeonSFML";
const int APPLICATION_FRAMERATE = 60;
sf::Clock g_fpsTimer;
const sf::Vector2f ZERO_VECTOR2F(0.0f, 0.0f);
const sf::Vector3f ZERO_VECTOR3F(0.0f, 0.0f, 0.0f);
const float PI = 3.14159265359;
const float PI_OVER_TWO = 1.570796326795;
const int MINIMAP_W = 150;
const int MINIMAP_H = MINIMAP_W;
const float ARENA_W = 1000.f;
const float ARENA_H = ARENA_W;
const float ARENA_VIEW_W = 1000.f;
const float ARENA_VIEW_H = 300.f;
const float ARENA_VIEW_ASPECT = ARENA_VIEW_W / ARENA_VIEW_H;
const float MAX_DIST_ZOOM = 1.5f;
const float MIN_DIST_ZOOM = 0.5f;
const float MAX_SHADOW_H = 100.0f;

float dtor(float degrees)
{
    return degrees * PI / 180.0f;
}
float rtod(float radians)
{
    return radians * 180.0f / PI;
}
sf::Vector2f rotateVector2f(const sf::Vector2f &v, float degrees)
{
    float radians = dtor(degrees);
    sf::Vector2f retVal(v.x*cos(radians)-v.y*sin(radians),
                        v.x*sin(radians)+v.y*cos(radians));
    return retVal;
}
float distanceVector2f(const sf::Vector2f &v0, const sf::Vector2f &v1)
{
    float retVal = sqrt(pow(v0.y-v1.y,2)+pow(v0.x-v1.x,2));
    return retVal;
}
float distanceVector3f(const sf::Vector3f &v0, const sf::Vector3f &v1)
{
    float retVal = sqrt(pow(v0.z-v1.z,2)+pow(v0.y-v1.y,2)+pow(v0.x-v1.x,2));
    return retVal;
}
float dotVector2f(const sf::Vector2f &v0, const sf::Vector2f &v1)
{
    return v0.x*v1.x + v0.y*v1.y;
}
float dotVector3f(const sf::Vector3f &v0, const sf::Vector3f &v1)
{
    return v0.x*v1.x + v0.y*v1.y + v0.z*v1.z;
}
float crossVector2f(const sf::Vector2f &v0, const sf::Vector2f &v1)
{
    return v0.x*v1.y - v0.y*v1.x;
}
float magnitudeVector2f(const sf::Vector2f &v)
{
    return sqrt(dotVector2f(v,v));
}
float magnitudeVector3f(const sf::Vector3f &v)
{
    return sqrt(dotVector3f(v,v));
}
float radiansVector2f(const sf::Vector2f &v0, const sf::Vector2f &v1)
{
    float magnitudes = magnitudeVector2f(v0) * magnitudeVector2f(v1);
    assert(magnitudes != 0.0f);
    return acos(dotVector2f(v0,v1) / magnitudes);
}
sf::Vector2f normalizeVector2f(const sf::Vector2f &v)
{
    float magnitude = magnitudeVector2f(v);
    assert(magnitude != 0.0f);
    return v / magnitude;
}
sf::Vector3f normalizeVector3f(const sf::Vector3f &v)
{
    float magnitude = magnitudeVector3f(v);
    assert(magnitude != 0.0f);
    return v / magnitude;
}

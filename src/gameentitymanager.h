#ifndef GAMEENTITYMANAGER_H
#define GAMEENTITYMANAGER_H

#include <map>
#include "globals.h"
#include "gameentity.h"
#include "gamecamera.h"

class GameEntityManager
{
    public:
        static const unsigned int MAX_ENTITIES;
    private:
        class CompareGameEntities
        {
            public:
                static sf::Vector2f debugCamPos;
                bool operator()(GameEntity* &e, GameEntity* &e2)
                {
                    sf::Vector2f ePos = sf::Vector2f(e->geometry.pos.x,e->geometry.pos.y);
                    sf::Vector2f e2Pos = sf::Vector2f(e2->geometry.pos.x,e2->geometry.pos.y);
                    float dist1 = distanceVector2f(ePos,debugCamPos);
                    float dist2 = distanceVector2f(e2Pos,debugCamPos);
                    if(dist1 < dist2)
                    {
                        return true;
                    }
                    return false;
                }
        };
    public:
        GameEntityManager();
        int getNextID();
        void addEntity(GameEntity* e);
        void clearEntities();
        void runEntities();
        void drawEntities(const GameCamera &camera);
    private:
        std::map<int, GameEntity*> entities;
        int currIndex;
};

#endif // GAMEENTITYMANAGER_H

#include "resourcemanager.h"
#include <sstream>

sf::Texture ResourceManager::BG_arena;
sf::Texture ResourceManager::DEBUG_boundsB;
sf::Texture ResourceManager::DEBUG_boundsF;
sf::Texture ResourceManager::minimapRing;
sf::Texture ResourceManager::minimapCam;
sf::Texture ResourceManager::minimapBG;
sf::Texture ResourceManager::gameEntityShadow;
sf::Texture ResourceManager::isaac_sword_attack_back[2];
sf::Texture ResourceManager::isaac_sword_attack_front[2];
sf::Texture ResourceManager::isaac_sword_back[3];
sf::Texture ResourceManager::isaac_sword_startcast_back;
sf::Texture ResourceManager::isaac_sword_cast_back[2];
sf::Texture ResourceManager::isaac_sword_startcast_front;
sf::Texture ResourceManager::isaac_sword_cast_front[2];
sf::Texture ResourceManager::isaac_sword_down_back;
sf::Texture ResourceManager::isaac_sword_down_front;
sf::Texture ResourceManager::isaac_sword_front[3];
sf::Texture ResourceManager::isaac_sword_hit_back;
sf::Texture ResourceManager::isaac_sword_hit_front;
sf::Font ResourceManager::FONT_arcade;

bool ResourceManager::load()
{
    if(!BG_arena.loadFromFile("resources/gfx/bgs/arena.png"))
    {
        return false;
    }
    if(!DEBUG_boundsB.loadFromFile("resources/gfx/debug_bounds_back.png"))
    {
        return false;
    }
    if(!DEBUG_boundsF.loadFromFile("resources/gfx/debug_bounds_front.png"))
    {
        return false;
    }
    if(!minimapRing.loadFromFile("resources/gfx/minimap_ring.png"))
    {
        return false;
    }
    if(!minimapCam.loadFromFile("resources/gfx/minimap_cam.png"))
    {
        return false;
    }
    if(!minimapBG.loadFromFile("resources/gfx/minimap_bg.png"))
    {
        return false;
    }
    if(!gameEntityShadow.loadFromFile("resources/gfx/entities/shadow.png"))
    {
        return false;
    }
    for(unsigned int c = 0; c < sizeof(isaac_sword_attack_back)/sizeof(isaac_sword_attack_back[0]); c++)
    {
        std::stringstream ss;
        ss << "resources/gfx/entities/isaac/sword_attack_back" << c << ".png";
        if(!isaac_sword_attack_back[c].loadFromFile(ss.str()))
        {
            return false;
        }
    }
    for(unsigned int c = 0; c < sizeof(isaac_sword_attack_front)/sizeof(isaac_sword_attack_front[0]); c++)
    {
        std::stringstream ss;
        ss << "resources/gfx/entities/isaac/sword_attack_front" << c << ".png";
        if(!isaac_sword_attack_front[c].loadFromFile(ss.str()))
        {
            return false;
        }
    }
    for(unsigned int c = 0; c < sizeof(isaac_sword_back)/sizeof(isaac_sword_back[0]); c++)
    {
        std::stringstream ss;
        ss << "resources/gfx/entities/isaac/sword_back_" << c << ".png";
        if(!isaac_sword_back[c].loadFromFile(ss.str()))
        {
            return false;
        }
    }
    if(!isaac_sword_startcast_back.loadFromFile("resources/gfx/entities/isaac/sword_cast_back0.png"))
    {
        return false;
    }
    for(unsigned int c = 0; c < sizeof(isaac_sword_cast_back)/sizeof(isaac_sword_cast_back[0]); c++)
    {
        std::stringstream ss;
        ss << "resources/gfx/entities/isaac/sword_cast_back1_" << c << ".png";
        if(!isaac_sword_cast_back[c].loadFromFile(ss.str()))
        {
            return false;
        }
    }
    if(!isaac_sword_startcast_front.loadFromFile("resources/gfx/entities/isaac/sword_cast_front0.png"))
    {
        return false;
    }
    for(unsigned int c = 0; c < sizeof(isaac_sword_cast_front)/sizeof(isaac_sword_cast_front[0]); c++)
    {
        std::stringstream ss;
        ss << "resources/gfx/entities/isaac/sword_cast_front1_" << c << ".png";
        if(!isaac_sword_cast_front[c].loadFromFile(ss.str()))
        {
            return false;
        }
    }
    if(!isaac_sword_down_back.loadFromFile("resources/gfx/entities/isaac/sword_downed_back.png"))
    {
        return false;
    }
    if(!isaac_sword_down_front.loadFromFile("resources/gfx/entities/isaac/sword_downed_front.png"))
    {
        return false;
    }
    for(unsigned int c = 0; c < sizeof(isaac_sword_front)/sizeof(isaac_sword_front[0]); c++)
    {
        std::stringstream ss;
        ss << "resources/gfx/entities/isaac/sword_front_" << c << ".png";
        if(!isaac_sword_front[c].loadFromFile(ss.str()))
        {
            return false;
        }
    }
    if(!isaac_sword_hit_back.loadFromFile("resources/gfx/entities/isaac/sword_hit_back.png"))
    {
        return false;
    }
    if(!isaac_sword_hit_front.loadFromFile("resources/gfx/entities/isaac/sword_hit_front.png"))
    {
        return false;
    }
    if(!FONT_arcade.loadFromFile("resources/font/ArcadeClassic.ttf"))
    {
        return false;
    }
    return true;
}

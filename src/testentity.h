#ifndef TESTENTITY_H
#define TESTENTITY_H

#include "gameentity.h"

class TestEntity : public GameEntity
{
    public:
        TestEntity(GameEntityManager &gem, sf::RenderWindow &w);
        virtual ~TestEntity();
        virtual void step();
        virtual void draw(const GameCamera &camera);
        virtual void collideWith(GameEntity *e);
    private:
        sf::Sprite sprite;
};

#endif // TESTENTITY_H

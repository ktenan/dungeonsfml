#include "testentity.h"
#include "resourcemanager.h"

TestEntity::TestEntity(GameEntityManager &gem, sf::RenderWindow &w) :
    GameEntity(gem,w,GameEntity::TYPE_ISAAC)
{
    sprite = sf::Sprite(ResourceManager::isaac_sword_front[0]);
    sprite.setOrigin(ResourceManager::isaac_sword_front[0].getSize().x/2.0f,
                     ResourceManager::isaac_sword_front[0].getSize().y);
}
TestEntity::~TestEntity()
{
}
void TestEntity::step()
{
}
void TestEntity::draw(const GameCamera &camera)
{
    sf::Vector2f rotPos = rotateVector2f(sf::Vector2f(geometry.pos.x, geometry.pos.y),-camera.rot);
    sprite.setPosition(rotPos.x, -rotPos.y / ARENA_VIEW_ASPECT);
    window.draw(sprite);
}
void TestEntity::collideWith(GameEntity *e)
{
}

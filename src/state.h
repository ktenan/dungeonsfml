#ifndef STATE_H
#define STATE_H

#include <SFML/Graphics.hpp>

class State
{
    public:
        enum StateID
        {
            STATE_GAME,
            NUM_STATES
        };
    public:
        State(sf::RenderWindow &w, State **s);
        virtual StateID run() = 0;
        virtual void draw() = 0;
        virtual void init() = 0;
        virtual void cleanup() = 0;
    protected:
        sf::RenderWindow &window;
        State **stateArray;
};

#endif // STATE_H

#include "gameunitisaac.h"
#include "resourcemanager.h"
#include <cmath>

const float GameUnitIsaac::RADIUS = 20.0f;

GameUnitIsaac::GameUnitIsaac(GameEntityManager &gem, sf::RenderWindow &w) :
    GameUnit(gem, w, GameEntity::TYPE_ISAAC),
    sprShadow(ResourceManager::gameEntityShadow),
    animSwordAttackBack(w, ResourceManager::isaac_sword_attack_back, 2, 10,
        sf::Vector2f(ResourceManager::isaac_sword_attack_back[0].getSize().x/2.0f,
                     ResourceManager::isaac_sword_attack_back[0].getSize().y)),
    animSwordAttackFront(w, ResourceManager::isaac_sword_attack_front, 2, 10,
        sf::Vector2f(ResourceManager::isaac_sword_attack_front[0].getSize().x/2.0f,
                     ResourceManager::isaac_sword_attack_front[0].getSize().y)),
    animSwordBack(w, ResourceManager::isaac_sword_back, 3, 10,
        sf::Vector2f(ResourceManager::isaac_sword_back[0].getSize().x/2.0f,
                     ResourceManager::isaac_sword_back[0].getSize().y), true),
    animSwordStartcastBack(w, &ResourceManager::isaac_sword_startcast_back, 1, 0,
        sf::Vector2f(ResourceManager::isaac_sword_startcast_back.getSize().x/2.0f,
                     ResourceManager::isaac_sword_startcast_back.getSize().y)),
    animSwordCastBack(w, ResourceManager::isaac_sword_cast_back, 2, 10,
        sf::Vector2f(ResourceManager::isaac_sword_cast_back[0].getSize().x/2.0f,
                     ResourceManager::isaac_sword_cast_back[0].getSize().y)),
    animSwordStartcastFront(w, &ResourceManager::isaac_sword_startcast_front, 1, 0,
        sf::Vector2f(ResourceManager::isaac_sword_startcast_front.getSize().x/2.0f,
                     ResourceManager::isaac_sword_startcast_front.getSize().y)),
    animSwordCastFront(w, ResourceManager::isaac_sword_cast_front, 2, 10,
        sf::Vector2f(ResourceManager::isaac_sword_cast_front[0].getSize().x/2.0f,
                     ResourceManager::isaac_sword_cast_front[0].getSize().y)),
    animSwordDownedBack(w, &ResourceManager::isaac_sword_down_back, 1, 1,
        sf::Vector2f(ResourceManager::isaac_sword_down_back.getSize().x/2.0f,
                     ResourceManager::isaac_sword_down_back.getSize().y)),
    animSwordDownedFront(w, &ResourceManager::isaac_sword_down_front, 1, 0,
        sf::Vector2f(ResourceManager::isaac_sword_down_front.getSize().x/2.0f,
                     ResourceManager::isaac_sword_down_front.getSize().y)),
    animSwordFront(w, ResourceManager::isaac_sword_front, 3, 10,
        sf::Vector2f(ResourceManager::isaac_sword_front[0].getSize().x/2.0f,
                     ResourceManager::isaac_sword_front[0].getSize().y), true),
    animSwordHitBack(w, &ResourceManager::isaac_sword_hit_back, 1, 0,
        sf::Vector2f(ResourceManager::isaac_sword_hit_back.getSize().x/2.0f,
                     ResourceManager::isaac_sword_hit_back.getSize().y)),
    animSwordHitFront(w, &ResourceManager::isaac_sword_hit_front, 1, 0,
        sf::Vector2f(ResourceManager::isaac_sword_hit_front.getSize().x/2.0f,
                     ResourceManager::isaac_sword_hit_front.getSize().y))
{
    geometry.broadphase_radius = RADIUS;
    sprShadow.setOrigin(ResourceManager::gameEntityShadow.getSize().x/2.0f,
                        ResourceManager::gameEntityShadow.getSize().y);
}
GameUnitIsaac::~GameUnitIsaac()
{
}
void GameUnitIsaac::step()
{
    GameUnit::step();
}
void GameUnitIsaac::draw(const GameCamera &camera)
{
    sf::Vector2f camHalfH = sf::Vector2f(0.0f, -camera.getSize().y);
    camHalfH = rotateVector2f(camHalfH, camera.rot);
    sf::Vector2f camBottom = camera.pos + camHalfH;
    sf::Vector2f screenDirection = camBottom - sf::Vector2f(geometry.pos.x,geometry.pos.y);
    Animation *currAnim = NULL;
    if(dotVector2f(screenDirection, sf::Vector2f(direction.x, direction.y)) >= 0.0f)
    {
        currAnim = &animSwordFront;
    }
    else
    {
        currAnim = &animSwordBack;
    }
    currAnim->flipX = crossVector2f(screenDirection, sf::Vector2f(direction.x, direction.y)) >= 0.0f;
    sf::Vector2f rotPos = rotateVector2f(sf::Vector2f(geometry.pos.x, geometry.pos.y),-camera.rot);
    currAnim->position = sf::Vector2f(rotPos.x, (-rotPos.y / ARENA_VIEW_ASPECT) - geometry.bottomZ() );
    float distToBottom = rotPos.y + ARENA_W;
    float bottomDistPercent = distToBottom / (ARENA_W*2);
    float zoomScale = MAX_DIST_ZOOM - (bottomDistPercent * (MAX_DIST_ZOOM-MIN_DIST_ZOOM));
    if(geometry.bottomZ() <= MAX_SHADOW_H)
    {
        float shadowPercent = 1.0f - (geometry.bottomZ() / MAX_SHADOW_H);
        sprShadow.setScale(shadowPercent*zoomScale, shadowPercent*zoomScale);
        sprShadow.setPosition(rotPos.x, -rotPos.y / ARENA_VIEW_ASPECT);
        window.draw(sprShadow);
    }
    currAnim->scale = sf::Vector2f(zoomScale,zoomScale);
    currAnim->animate();
    currAnim->draw();
}
void GameUnitIsaac::collideWith(GameEntity *e)
{
    GameUnit::collideWith(e);
}

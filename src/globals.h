#ifndef GLOBALS_H_INCLUDED
#define GLOBALS_H_INCLUDED

#include <string>
#include <SFML/System.hpp>

extern const int          WINDOW_W;
extern const int          WINDOW_H;
extern const float        WINDOW_ASPECT;
extern const std::string  WINDOW_TITLE;
extern const int          APPLICATION_FRAMERATE;
extern       sf::Clock    g_fpsTimer;
extern const sf::Vector2f ZERO_VECTOR2F;
extern const sf::Vector3f ZERO_VECTOR3F;
extern const float        PI;
extern const float        PI_OVER_TWO;
//gamestate consts
extern const int MINIMAP_W;
extern const int MINIMAP_H;
extern const float ARENA_W;
extern const float ARENA_H;
extern const float ARENA_VIEW_W;
extern const float ARENA_VIEW_H;
extern const float ARENA_VIEW_ASPECT;
extern const float MAX_DIST_ZOOM;
extern const float MIN_DIST_ZOOM;
extern const float MAX_SHADOW_H;

float dtor(float degrees);
float rtod(float radians);
sf::Vector2f rotateVector2f(const sf::Vector2f &v, float degrees);
float distanceVector2f(const sf::Vector2f &v0, const sf::Vector2f &v1);
float distanceVector3f(const sf::Vector3f &v0, const sf::Vector3f &v1);
float dotVector2f(const sf::Vector2f &v0, const sf::Vector2f &v1);
float dotVector3f(const sf::Vector3f &v0, const sf::Vector3f &v1);
float crossVector2f(const sf::Vector2f &v0, const sf::Vector2f &v1);
float magnitudeVector2f(const sf::Vector2f &v);
float magnitudeVector3f(const sf::Vector3f &v);
float radiansVector2f(const sf::Vector2f &v0, const sf::Vector2f &v1);
sf::Vector2f normalizeVector2f(const sf::Vector2f &v);
sf::Vector3f normalizeVector3f(const sf::Vector3f &v);

#endif // GLOBALS_H_INCLUDED

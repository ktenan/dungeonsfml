#include "gameentitymanager.h"
#include <cassert>
#include <vector>
#include <queue>

const unsigned int GameEntityManager::MAX_ENTITIES = 0xFFFF;
sf::Vector2f GameEntityManager::CompareGameEntities::debugCamPos;

GameEntityManager::GameEntityManager() :
    currIndex(0)
{
}
int GameEntityManager::getNextID()
{
    if(entities.size() >= MAX_ENTITIES)
    {
        return -1;
    }
    while(entities.find(currIndex) != entities.end())
    {
        currIndex++;
        if(currIndex >= (int)MAX_ENTITIES)
        {
            currIndex = 0;
        }
    }
    return currIndex;
}
void GameEntityManager::addEntity(GameEntity* e)
{
    assert(entities.size() < MAX_ENTITIES);
    assert(entities.find(e->getID()) == entities.end());
    entities[e->getID()] = e;
}
void GameEntityManager::clearEntities()
{
    std::map<int, GameEntity*>::iterator it = entities.begin();
    for(; it != entities.end(); it++)
    {
        delete it->second;
    }
    entities.clear();
    currIndex = 0;
}
void GameEntityManager::runEntities()
{
    std::map<int, GameEntity*>::iterator it = entities.begin();
    std::map<int, GameEntity*>::iterator it2;
    std::vector<int> nullifyList;
    for(; it != entities.end(); it++)
    {
        GameEntity *e = it->second;
        e->step();
        ///TODO: make this not suck
        for(it2 = entities.begin(); it2 != entities.end(); it2++)
        {
            if(e->getID() == it2->first)
            {
                continue;
            }
            GameEntity *e2 = it2->second;
            if(e->isCollidingWith(e2))
            {
                e->collideWith(e2);
            }
        }
        if(e->nullify)
        {
            nullifyList.push_back(e->getID());
        }
    }
    for(unsigned int c = 0; c < nullifyList.size(); c++)
    {
        entities.erase(nullifyList[c]);
    }
}
void GameEntityManager::drawEntities(const GameCamera &camera)
{
    sf::Vector2f camHalfH = sf::Vector2f(0.0f, -camera.getSize().y);
    camHalfH = rotateVector2f(camHalfH, camera.rot);
    CompareGameEntities::debugCamPos = camera.pos + camHalfH;
    std::priority_queue<GameEntity*, std::vector<GameEntity*>, CompareGameEntities> pq;
    std::map<int, GameEntity*>::iterator it = entities.begin();
    for(; it != entities.end(); it++)
    {
        pq.push(it->second);
    }
    while(!pq.empty())
    {
        GameEntity* e = pq.top();
        e->draw(camera);
        pq.pop();
    }
}

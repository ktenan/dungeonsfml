#include "gamestate.h"
#include "resourcemanager.h"
#include "testentity.h"

GameState::GameState(sf::RenderWindow &w, State **s) :
    State(w,s)
{
}
State::StateID GameState::run()
{
    StateID retVal = State::STATE_GAME;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::I))
    {
        camera.pos += rotateVector2f(sf::Vector2f(0.0f,10.0f),camera.rot);
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::J))
    {
        camera.pos -= rotateVector2f(sf::Vector2f(10.0f,0.0f),camera.rot);
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::K))
    {
        camera.pos -= rotateVector2f(sf::Vector2f(0.0f,10.0f),camera.rot);
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::L))
    {
        camera.pos += rotateVector2f(sf::Vector2f(10.0f,0.0f),camera.rot);
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::U))
    {
        camera.rot += 2.1f;//CW
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::O))
    {
        camera.rot -= 2.1f;//CCW
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::E))
    {
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::F))
    {
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
    {
        retVal = State::NUM_STATES;
    }
    gem.runEntities();
    return retVal;
}
void GameState::draw()
{
    drawBG();
    drawMainScene();
    drawMinimap();
}
void GameState::init()
{
    mainCam = window.getDefaultView();
    bgCam = window.getDefaultView();
    minimapCam.reset(sf::FloatRect(0,0,MINIMAP_W,MINIMAP_H));
    float minimapW = minimapCam.getSize().x / mainCam.getSize().x;
    float minimapH = minimapCam.getSize().y / mainCam.getSize().y;
    minimapCam.setViewport(sf::FloatRect(1.0f-minimapW,0.0f,minimapW,minimapH));
    camera.rot = 0.0f;
    camera.pos = ZERO_VECTOR2F;
    camera.setW(WINDOW_W);
    sprBG = sf::Sprite(ResourceManager::BG_arena);
    sprDebugBoundsB = sf::Sprite(ResourceManager::DEBUG_boundsB);
    sprDebugBoundsB.setColor(sf::Color(255,255,255,127));
    sprDebugBoundsF = sf::Sprite(ResourceManager::DEBUG_boundsF);
    sprDebugBoundsF.setColor(sf::Color(255,255,255,127));
    sprMinimapRing = sf::Sprite(ResourceManager::minimapRing);
    sprMinimapCam = sf::Sprite(ResourceManager::minimapCam);
    sprMinimapCam.setOrigin(MINIMAP_W/2.0f,MINIMAP_H/2.0f);
    sprMinimapCam.setColor(sf::Color(255,255,255,127));
    sprMinimapBG = sf::Sprite(ResourceManager::minimapBG);
    isaac = new GameUnitIsaac(gem, window);
    isaac->geometry.pos.x = -ARENA_W / 4.0f;
}
void GameState::cleanup()
{
    gem.clearEntities();
}
void GameState::drawBG()
{
    ///TODO: set max camSize?
    //camPos uses the cameras target position in the arena
    //arena dimensions are 1000x300 I think...
    //static const sf::Vector2f MAX_CAM_SIZE(1000,724); // or not?...
    sf::Vector2u bgSize = sprBG.getTexture()->getSize();
    bgCam.setCenter(bgSize.x/2, bgSize.y/2);
    float clampedCamRot = camera.rot - ((int)(camera.rot / 180.0f) * 180.0f);
    float scrollPercent = clampedCamRot / 180.0f;
    bgCam.move(bgSize.x * scrollPercent * -1, 0.0f);
    sf::Vector2f rotCamPos = rotateVector2f(camera.pos,-camera.rot);
    bgCam.move(rotCamPos.x, -rotCamPos.y / ARENA_VIEW_ASPECT);
    bgCam.setSize(camera.getSize().x, camera.getSize().y / ARENA_VIEW_ASPECT);
    window.setView(bgCam);
    ///TODO: make a legit drawing section for two instead of drawing all these...
    for(int c = -3 * (int)bgSize.x; c < 3 * (int)bgSize.x; c += (int)bgSize.x)
    {
        sprBG.setPosition(c,0);
        window.draw(sprBG);
    }
}
void GameState::drawMainScene()
{
    sf::Vector2u bgSize = sprBG.getTexture()->getSize();
    mainCam.setCenter(bgSize.x/2, bgSize.y/2);
    sf::Vector2f rotCamPos = rotateVector2f(camera.pos,-camera.rot);
    mainCam.move(rotCamPos.x, -rotCamPos.y / ARENA_VIEW_ASPECT);
    mainCam.setSize(camera.getSize().x, camera.getSize().y / ARENA_VIEW_ASPECT);
    window.setView(mainCam);
    window.draw(sprDebugBoundsB);
    mainCam.move((int)bgSize.x/-2, (int)bgSize.y/-2);
    window.setView(mainCam);
    gem.drawEntities(camera);
    mainCam.move((int)bgSize.x/2, (int)bgSize.y/2);
    window.setView(mainCam);
    window.draw(sprDebugBoundsF);
}

void GameState::drawMinimap()
{
    window.setView(minimapCam);
    window.draw(sprMinimapBG);
    window.draw(sprMinimapRing);
    sprMinimapCam.setPosition(MINIMAP_W/2.0f,MINIMAP_H/2.0f);
    sprMinimapCam.setRotation(-camera.rot);
    sf::Vector2f tempPos = camera.pos;
    tempPos.y *= -1;
    tempPos /= ARENA_W;
    tempPos *= (float)MINIMAP_W;
    sprMinimapCam.move(tempPos);
    sprMinimapCam.setScale(camera.getSize() / ARENA_W);
    window.draw(sprMinimapCam);
}

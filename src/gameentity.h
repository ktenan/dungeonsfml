#ifndef GAMEENTITY_H
#define GAMEENTITY_H

#include <SFML/Graphics.hpp>
#include "gameentitygeometry.h"
#include "gamecamera.h"
class GameEntityManager;

class GameEntity
{
    public:
        enum Type
        {
            TYPE_UNIT,
                TYPE_ISAAC,
                END_TYPE_ISAAC,
            END_TYPE_UNIT
        };
    public:
        GameEntity(GameEntityManager &gem, sf::RenderWindow &w, Type t);
        virtual ~GameEntity();
        virtual void step() = 0;
        virtual void draw(const GameCamera &camera) = 0;
        virtual void collideWith(GameEntity *e) = 0;
        int getID();
        Type getType();
        bool isCollidingWith(GameEntity *e);
    public:
        bool nullify;
        GameEntityGeometry geometry;
    protected:
        sf::RenderWindow &window;
        GameEntityManager &entityManager;
    private:
        int id;
        Type type;
};

#endif // GAMEENTITY_H

#ifndef GAMECAMERA_H
#define GAMECAMERA_H

#include "globals.h"

class GameCamera
{
    public:
        const sf::Vector2f& getSize() const;
        void setW(float w);
        void setH(float h);
    public:
        float rot;//degrees
        sf::Vector2f pos;
    private:
        sf::Vector2f size;
};

#endif // GAMECAMERA_H

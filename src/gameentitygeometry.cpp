#include "gameentitygeometry.h"
#include "globals.h"

bool GameEntityGeometry::testBroadPhase(GameEntityGeometry &g)
{
    float distance = distanceVector3f(pos, g.pos);
    if(distance <= broadphase_radius + g.broadphase_radius)
    {
        return true;
    }
    return false;
}

float GameEntityGeometry::bottomZ()
{
    return pos.z - broadphase_radius;
}

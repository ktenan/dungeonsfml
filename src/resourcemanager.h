#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include <SFML/Graphics.hpp>

class ResourceManager
{
    public:
        static bool load();
    public:
        static sf::Texture BG_arena;
        static sf::Texture DEBUG_boundsB;
        static sf::Texture DEBUG_boundsF;
        static sf::Texture minimapRing;
        static sf::Texture minimapCam;
        static sf::Texture minimapBG;
        static sf::Texture gameEntityShadow;
        static sf::Texture isaac_sword_attack_back[2];
        static sf::Texture isaac_sword_attack_front[2];
        static sf::Texture isaac_sword_back[3];
        static sf::Texture isaac_sword_startcast_back;
        static sf::Texture isaac_sword_cast_back[2];
        static sf::Texture isaac_sword_startcast_front;
        static sf::Texture isaac_sword_cast_front[2];
        static sf::Texture isaac_sword_down_back;
        static sf::Texture isaac_sword_down_front;
        static sf::Texture isaac_sword_front[3];
        static sf::Texture isaac_sword_hit_back;
        static sf::Texture isaac_sword_hit_front;
        static sf::Font FONT_arcade;
};

#endif // RESOURCEMANAGER_H

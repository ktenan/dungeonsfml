#include "animation.h"

Animation::Animation(sf::RenderWindow &w, sf::Texture *texArray, int numFrames, int ticksPF, sf::Vector2f origin, bool oscill) :
    flipX(false),
    scale(1.0f, 1.0f),
    window(w),
    frames(numFrames),
    currFrame(0),
    ticksPerFrame(ticksPF),
    ticksLeft(ticksPF),
    oscillate(oscill),
    oscillateDirection(true)
{
    for(int c = 0; c < frames; c++)
    {
        sf::Sprite spr(texArray[c]);
        spr.setOrigin(origin);
        sprites.push_back(spr);
    }
}
void Animation::animate()
{
    ticksLeft--;
    if(ticksLeft < 0)
    {
        ticksLeft = ticksPerFrame;
        if(oscillate)
        {
            if(oscillateDirection)
            {
                currFrame++;
                if(currFrame >= frames)
                {
                    currFrame = frames - 2;
                    oscillateDirection = false;
                }
            }
            else
            {
                currFrame--;
                if(currFrame < 0)
                {
                    currFrame = 1;
                    oscillateDirection = true;
                }
            }
        }
        else
        {
            currFrame++;
            if(currFrame >= frames)
            {
                currFrame = 0;
            }
        }
    }
}
void Animation::draw()
{
    sprites[currFrame].setPosition(position);
    if(flipX)
    {
        sprites[currFrame].setScale(-1.0f*scale.x, scale.y);
    }
    else
    {
        sprites[currFrame].setScale(scale);
    }
    window.draw(sprites[currFrame]);
}

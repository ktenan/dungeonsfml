#include "gameentity.h"
#include "gameentitymanager.h"

GameEntity::GameEntity(GameEntityManager &gem, sf::RenderWindow &w, Type t) :
    nullify(false),
    window(w),
    entityManager(gem),
    type(t)
{
    int nextID = gem.getNextID();
    id = nextID;
    if(nextID >= 0)
    {
        gem.addEntity(this);
    }
}
GameEntity::~GameEntity()
{
}
int GameEntity::getID()
{
    return id;
}
GameEntity::Type GameEntity::getType()
{
    return type;
}
bool GameEntity::isCollidingWith(GameEntity *e)
{
    if(geometry.testBroadPhase(e->geometry))
    {
        return true;
    }
    ///TODO: narrow phase
    return false;
}
